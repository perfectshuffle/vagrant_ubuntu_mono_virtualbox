#!/bin/bash

npm install -g grunt-cli bower gulp foundation-cli

# Install gems if not already installed
if ! gem spec compass > /dev/null 2>&1; then
  gem install compass
fi

if ! gem spec sass > /dev/null 2>&1; then
  gem install sass
fi

# Zurb foundation 5
if ! gem spec foundation > /dev/null 2>&1; then
  gem install foundation
fi

if ! gem spec bootstrap-sass > /dev/null 2>&1; then
  gem install bootstrap-sass --pre
fi

npm install -g LiveScript
npm install -g mimosa
npm install -g mimosa-livescript
npm install -g mimosa-sass
npm install -g purescript
npm install -g elm

# Stops some encoding issues with compass/sass
# https://github.com/csswizardry/inuit.css/issues/270
locale-gen en_US en_US.UTF-8
dpkg-reconfigure locales
update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
echo export LC_ALL="en_US.UTF-8" >> /etc/bash.bashrc
echo export LC_CTYPE="en_US.UTF-8" >> /etc/bash.bashrc
echo export ELM_HOME="/usr/local/lib/node_modules/elm/share" >> /etc/bash.bashrc

cd /vagrant/www
sudo -u vagrant bundle install
sudo -u vagrant bower install --config.interactive=false bower.json

exit 0

