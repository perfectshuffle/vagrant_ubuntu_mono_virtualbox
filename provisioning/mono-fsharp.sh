#!/bin/sh

# apt-get install -y mono-complete fsharp libfsharp-core4.3-cil libfsharp-data-typeproviders4.3

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
sudo apt-get update
apt-get install -y mono-complete fsharp autoconf libtool pkg-config make git automake
