#!/bin/sh

# docker ubuntu image unhelpfully forbids init.d restarts unless you overwrite this policy...
echo "#!/bin/sh\nexit 0" > /usr/sbin/policy-rc.d

apt-get install -y nginx mono-fastcgi-server4 mono-xsp4

/etc/init.d/mono-xsp4 stop

cp /vagrant/provisioning/monoserve /etc/init.d

mkdir -p /etc/mono/fcgi/apps-available
mkdir -p /etc/mono/fcgi/apps-enabled

cp /vagrant/provisioning/www /etc/mono/fcgi/apps-enabled/www

chmod +x /etc/init.d/monoserve
update-rc.d monoserve defaults

sed -ie 's|mono/4.0|mono/4.5|g' /usr/bin/fastcgi-mono-server4
sed -ie 's|mono/4.0|mono/4.5|g' /usr/bin/xsp4

cp /vagrant/provisioning/nginx/default /etc/nginx/sites-enabled/default

# This fixes an argumentnullexception when loading pages
cat /vagrant/provisioning/fastcgi_params.txt >> /etc/nginx/fastcgi_params

/etc/init.d/monoserve restart
/etc/init.d/nginx restart


