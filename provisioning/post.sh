#!/bin/sh

cd /vagrant

sudo -H -u vagrant tmux new-session -d "teamocil --layout tmuxlayout.yml"

# Just so we can easily see IP address etc
ifconfig

echo "Run 'tmux attach-session' to see server dashboard"
echo "All done :)"
