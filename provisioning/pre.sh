#!/bin/sh

apt-get update
apt-get upgrade -y
apt-get install -y build-essential git nano tmux software-properties-common dos2unix

# Contains ruby 2.1
add-apt-repository -y ppa:brightbox/ruby-ng

# Contains glibcxx (needed for F#/mono) : http://askubuntu.com/questions/575505/glibcxx-3-4-20-not-found-how-to-fix-this-error
add-apt-repository -y ppa:ubuntu-toolchain-r/test 

apt-get update

apt-get -y install libstdc++6 ruby2.2 ruby2.2-dev npm nodejs nodejs-legacy

# Install gem if not already installed
if ! gem spec bundle > /dev/null 2>&1; then
  gem install bundle
fi

# Install gem if not already installed
if ! gem spec teamocil > /dev/null 2>&1; then
  gem install teamocil
fi


